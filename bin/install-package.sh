#!/bin/bash

# This script will install all dependencies
find . -maxdepth 3 -name package.json -execdir npm i \;