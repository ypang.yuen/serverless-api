#!/bin/bash
. ./bin/utils.sh

SOURCE_FILE="./envs/${COUNTRY_CODE}.local.env" ;
PARAM=""
echo "SAM Overrided Parameters:"
verifyEnv "$SOURCE_FILE";
getEnv "$SOURCE_FILE";
printSeparator

# overwrite DB URI with SSH tunnel, change port depending on your setting
DB_HOST=docker.for.mac.localhost
DB_PORT=13306
API_REGION=ap-southeast-1

PROJECT_PATH=$(getProjectPath $COUNTRY_CODE)

cd $PROJECT_PATH;
echo "Initiating SAM Package:";
# Deploy packaged code.
sam local start-api -t apis.sam.local.yaml \
  --parameter-overrides \
  $PARAM
