#!/bin/bash
VALID_COUNTRIES=("jp" "th" "id" "hk" "kh");
VALID_ENVIRONMENTS=("dev" "uat" "prod");
VALID_VERIFICATION_RESPONSES=("Y" "N" "y" "n");

printSeparator() {
    echo ""
    echo "--------------------------------------------------------------------------------------------------------------"
    echo "--------------------------------------------------------------------------------------------------------------"
    echo ""
}

printListItem() {
    TEXT="${1}";
    CHECK_MARK="*******";
    echo "${CHECK_MARK} ${TEXT}";
}

camelcase() {
  echo $(echo $1 | awk '{ print tolower($1) }' | awk -F _ '{for(i=1; i<=NF; i++) printf "%s", toupper(substr($i,1,1)) substr($i,2); print"";}')
}

getSecretVersion() {
  secret="${1}" ;
  region="${2}" ;
  profile="${3}" ;
  if [[ ${profile} != "" ]]; then
    echo $(aws secretsmanager list-secret-version-ids --secret-id "${1}" --region "${2}" --profile ${3}\
    | jq .Versions | jq -c ".[] | select(.VersionStages | contains([\"AWSCURRENT\"]))" | jq -cr .VersionId);
  else
    echo $(aws secretsmanager list-secret-version-ids --secret-id "${1}" --region "${2}"\
	| jq .Versions | jq -c ".[] | select(.VersionStages | contains([\"AWSCURRENT\"]))" | jq -cr .VersionId);
  fi
}

getEnv() {
	source="${1}" ;
	while IFS='=' read -r n v || [ -n "$n" ]; do
		PARAM+="ParameterKey=$(camelcase $n),ParameterValue=$v " ;
	done < $source ; 
	PARAM+="ParameterKey=SecretVersionId,ParameterValue=$SECRET_VERSION_ID " ; 
	PARAM+="ParameterKey=SecretName,ParameterValue=$SECRET_NAME " ; 
	PARAM+="ParameterKey=Country,ParameterValue=$COUNTRY_CODE " ; 
	PARAM+="ParameterKey=Environment,ParameterValue=$ENVIRONMENT_NAME" ; 
}

verifyEnv() {
  source="${1}" ;
	while IFS='=' read -r n v || [ -n "$n" ]; do
		printListItem "$(camelcase $n) = $v " ;
	done < $source ; 
}

getProjectPath() {
  countryCode="${1}" ;
  if [ $countryCode == "kh" ]; then
    echo "002_apis_kh"
  elif [ $countryCode == "th" ]; then
    echo "002_apis"
  else 
    printListItem "Invalid PROJECT_PATH";
    exit 0;
  fi
}


collectCountry() {
  echo "Enter Country Code (${VALID_COUNTRIES[*]}): "
  read COUNTRY_CODE
}

collectEnvironment() {
  echo "Enter Environment Code (${VALID_ENVIRONMENTS[*]}): "
  read ENVIRONMENT_NAME
}

collectAwsCliProfile() {
  echo "Enter AWS CLI Profile: "
  read AWS_CLI_PROFILE
}

collectProjectPath() {
  echo "Enter Project Path: "
  read PROJECT_PATH
}