resource "aws_dynamodb_table" "th_claim_snapshot" {
  name         = "th-dev-claim-snapshot"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "userId"
  range_key    = "caseNo"

  attribute {
    name = "userId"
    type = "S"
  }

  attribute {
    name = "caseNo"
    type = "S"
  }

  server_side_encryption {
    enabled = true
  }

  tags = {
    Name        = "th-local-claim-snapshot"
    Environment = "local"
  }
}
